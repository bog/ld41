#ifndef MENUNODE_HPP
#define MENUNODE_HPP
#include <iostream>
#include <Core.hpp>

class MenuNode : public Node
{
public:
  MenuNode();
  
  virtual void draw(sf::RenderTarget& target,
		    sf::RenderStates states) const override;

  virtual void update(float dt) override;
  virtual void on_event(sf::Event event) override;

  virtual ~MenuNode();
  
protected:
  sf::RectangleShape m_shape;
private:
  MenuNode( MenuNode const& menunode ) = delete;
  MenuNode& operator=( MenuNode const& menunode ) = delete;
};

#endif
