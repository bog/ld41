#include <EndNode.hpp>
#include <MenuNode.hpp>

EndNode::EndNode()
  : Node("END")
{
  m_shape.setSize({$(winsize).x, $(winsize).y});
  m_shape.setTexture($(textures).pull("end"));
}

void EndNode::draw(sf::RenderTarget& target,
		   sf::RenderStates states) const
{
  target.draw(m_shape);
}
     
void EndNode::update(float dt)
{
}

void EndNode::on_event(sf::Event event)
{
  if (event.type == sf::Event::KeyPressed)
    {
      if (event.key.code == sf::Keyboard::Space)
	{
	  $(musics).pull("shoot")->stop();
	  Core::instance().scene(new MenuNode);
	}
      if (event.key.code == sf::Keyboard::Escape)
	{
	  $(musics).pull("shoot")->stop();
	  $(quit);
	}
    }
}


EndNode::~EndNode()
{
  
}
