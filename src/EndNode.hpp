#ifndef ENDNODE_HPP
#define ENDNODE_HPP
#include <iostream>
#include <Core.hpp>

class EndNode : public Node
{
public:
  EndNode();

  virtual void draw(sf::RenderTarget& target,
		    sf::RenderStates states) const override;

  virtual void update(float dt) override;
  virtual void on_event(sf::Event event) override;

  virtual ~EndNode();
  
protected:
  sf::RectangleShape m_shape;
private:
  EndNode( EndNode const& endnode ) = delete;
  EndNode& operator=( EndNode const& endnode ) = delete;
};

#endif
