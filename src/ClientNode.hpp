#ifndef CLIENTNODE_HPP
#define CLIENTNODE_HPP
#include <iostream>
#include <Core.hpp>
#include <ActorNode.hpp>

enum class ClientState {
  Arrive,
    Wait,
    Leave,
    Dead,
    Flee
};

class ClientNode : public ActorNode
{
public:
  ClientNode(LevelNode& level, std::vector<std::unique_ptr<ClientNode>>& others);
  void update(float dt) override;
  void update_flee(float dt);
  void update_avoid(float dt);
  void update_collisions(float dt);
  void update_leave(float dt);
  void update_wait(float dt);
  void update_arrive(float dt);
  void draw(sf::RenderTarget& target,
	    sf::RenderStates states) const override;

  void wait_more() { m_timer_wait = 0.0f; }
  void happy();
  void flee() { m_state = ClientState::Flee; }
  ClientState state() const { return m_state; }
  virtual ~ClientNode();

  bool live() const { return m_live; }
  bool alive() const { return m_hp > 0; }
  
  int document() const { return m_document; }
  void document(int doc) { m_document = doc; }
  
protected:
  std::vector<std::unique_ptr<ClientNode>>& m_others;
  float m_avoid_dist;
  bool m_live;
  ClientState m_state;
  float m_time_wait;
  float m_timer_wait;
  sf::RectangleShape m_wait_shape;
  int m_document;
  float m_time_flee;
  float m_timer_flee;
  float m_angle;
  float m_timer_stay;
  float m_time_stay;
private:
  ClientNode( ClientNode const& clientnode ) = delete;
  ClientNode& operator=( ClientNode const& clientnode ) = delete;
};

#endif
