#ifndef ANIMATION_HPP
#define ANIMATION_HPP
#include <iostream>
#include <Core.hpp>

class Animation : public Node
{
public:
  Animation(sf::RectangleShape& shape);
  void setup(int row, int col_start, int col_end, float time);
  void draw(sf::RenderTarget& target,
	    sf::RenderStates states) const override;
  
  void update(float dt) override;

  void row(int r) { m_row = r; }
  
  virtual ~Animation();
  
protected:
  sf::RectangleShape& m_shape;
  int m_row;
  int m_col_start;
  int m_col_end;
  float m_time;
  float m_timer;
  int m_frame;
private:
  Animation( Animation const& animation ) = delete;
  Animation& operator=( Animation const& animation ) = delete;
};

#endif
