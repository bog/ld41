#ifndef SHOOTLEVEL_HPP
#define SHOOTLEVEL_HPP
#include <iostream>
#include <Core.hpp>

class LevelNode;
class ClientNode;
class AdminLevel;

class ShootLevel : public Node
{
public:
  ShootLevel(LevelNode& level, AdminLevel& admin);
  virtual void draw(sf::RenderTarget& target,
		    sf::RenderStates states) const override ;

  virtual void update(float dt) override;
  virtual void on_event(sf::Event event) override;

  glm::vec2 shoot_line(glm::vec2 p1, glm::vec2 p2, sf::Color color);
  void shoot_impact(glm::vec2 pos);

  void restore_player_pos();
  bool ready_to_admin();
  void new_shoot_mode() { ShootLevel::ShootCount++; }
  int shoot_count() const { return ShootLevel::ShootCount; }
  static void reset_shoot_count() { ShootLevel::ShootCount = 0; }
  virtual ~ShootLevel();
  
protected:
  static int ShootCount;
  LevelNode& m_level;
  float m_time_shoot;
  float m_timer_shoot;
  bool m_visible_shoot;
  sf::VertexArray m_shoot_line;
  float m_shoot_length;
  sf::CircleShape m_shoot_impact;
  std::vector<std::unique_ptr<ClientNode>> m_clients;
  int m_client_count;
  glm::vec2 m_player_old_pos;
  float m_frustration_decrease;
  float m_timer_end;
  float m_time_end;
  sf::Sound m_gun_sound;
  sf::Sound m_reload_sound;
  sf::Sound m_die_sound;

private:
  ShootLevel( ShootLevel const& shootlevel ) = delete;
  ShootLevel& operator=( ShootLevel const& shootlevel ) = delete;
};

#endif
