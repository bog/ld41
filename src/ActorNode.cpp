#include <ActorNode.hpp>
#include <Animation.hpp>

ActorNode::ActorNode(std::string name, LevelNode& level)
  : Node(name)
  , m_level (level)
{
  m_max_vel = 256;
  m_move_acc = 204800.0f;
  m_size = {32.0f, 32.0f};

  m_max_hp = 100.0f;
  m_hp = m_max_hp;
  
  m_anim = std::unique_ptr<Animation>(new Animation(m_shape));
}

void ActorNode::draw(sf::RenderTarget& target,
		     sf::RenderStates states) const
{
  m_anim->draw(target, states);
}

void ActorNode::apply_force(glm::vec2 force)
{
  m_acc += force;
}

void ActorNode::update(float dt)
{
  float speed = glm::length(m_velocity);

  if (speed > 0)
    {
      glm::vec2 drag =
	-0.5f *
	glm::normalize(m_velocity) *
	speed *
	speed;

      if (speed > 10.0f)
	{
	  apply_force(drag);
	}
      else
	{
	  m_velocity = {0, 0};
	}
    }
  
  m_position += m_velocity * dt;
  m_velocity += m_acc * dt;
  m_velocity = glm::clamp(m_velocity, -m_max_vel, m_max_vel);

  m_acc = {0, 0};

  // Graphics
  m_anim->update(dt);
  m_shape.setPosition({m_position.x - m_size.x/2, m_position.y - m_size.y/2});
  m_shape.setSize({m_size.x, m_size.y});
}

void ActorNode::hurt(float damages)
{
  m_hp -= damages;
  if (m_hp < 0) { m_hp = 0; }
}

ActorNode::~ActorNode()
{
  
}
