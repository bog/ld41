#include <iostream>
#include <SFML/Graphics.hpp>
#include <Core.hpp>

int main(int argc, char** argv)
{
  sf::RenderWindow window(sf::VideoMode(640, 480), "LD41");
  
  Core& core = Core::instance();
  core.run(&window);

  Core::destroy();
  return 0;
}
