#include <PlayerNode.hpp>
#include <Animation.hpp>
#include <TileNode.hpp>

PlayerNode::PlayerNode(LevelNode& level)
  : ActorNode("PLAYER", level)
{
  m_position = {10*TILE_W, 13*TILE_H};
  m_shoot_dir = {0, -1};
  
  $(input).add_action("move_up", sf::Keyboard::Z);
  $(input).add_action("move_up", sf::Keyboard::W);
  $(input).add_action("move_down", sf::Keyboard::S);
  $(input).add_action("move_left", sf::Keyboard::Q);
  $(input).add_action("move_left", sf::Keyboard::A);
  $(input).add_action("move_right", sf::Keyboard::D);
  
  $(input).add_action("shoot_up", sf::Keyboard::Up);
  $(input).add_action("shoot_down", sf::Keyboard::Down);
  $(input).add_action("shoot_left", sf::Keyboard::Left);
  $(input).add_action("shoot_right", sf::Keyboard::Right);
  m_anim->setup(2, 0, 0, 0.2f);
}

void PlayerNode::update(float dt)
{  
  if ( $(input).action("move_up") )
    {
      apply_force({0, -m_move_acc});
      m_anim->setup(3, 0, 1, 0.2f);
    }

  if ( $(input).action("move_down") )
    {
      apply_force({0, m_move_acc});
      m_anim->setup(5, 0, 1, 0.2f);
    }

  if ( $(input).action("move_left") )
    {
      apply_force({-m_move_acc, 0});
      m_anim->setup(6, 0, 1, 0.2f);
    }

  if ( $(input).action("move_right") )
    {
      apply_force({m_move_acc, 0});
      m_anim->setup(4, 0, 1, 0.2f);
    }

  if (glm::length(m_velocity) == 0)
    {
      m_anim->setup(2, 0, 0, 0.2f);
    }
  
  update_shoot(dt);
  
  ActorNode::update(dt);
}

void PlayerNode::update_shoot(float dt)
{
  m_shoot_dir = {0, 0};
  
  if ( $(input).action("shoot_up") ) { m_shoot_dir.y = -1; }
  if ( $(input).action("shoot_down") ) { m_shoot_dir.y = 1; }
  if ( $(input).action("shoot_left") ) { m_shoot_dir.x = -1; }
  if ( $(input).action("shoot_right") ) { m_shoot_dir.x = 1; }
  
  if (glm::length(m_shoot_dir) == 0) { m_shoot_dir = {0, -1}; }
  
  m_shoot_dir = glm::normalize(m_shoot_dir);  
}

PlayerNode::~PlayerNode()
{
  
}
