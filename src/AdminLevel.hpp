#ifndef ADMINLEVEL_HPP
#define ADMINLEVEL_HPP
#include <iostream>
#include <Core.hpp>

class LevelNode;
class ClientNode;

class AdminLevel : public Node
{
public:
  AdminLevel(LevelNode& level);
  void draw(sf::RenderTarget& target,
		    sf::RenderStates states) const override;

  void update_request(float dt);
  void update(float dt) override;
  void add_client();
  void on_event(sf::Event event) override;
  std::vector<std::unique_ptr<ClientNode>>& clients() { return m_clients; }
  void increase_difficulty();
  virtual ~AdminLevel();
  
protected:
  LevelNode& m_level;
  glm::vec2 m_spawn_pos;
  std::vector<std::unique_ptr<ClientNode>> m_clients;
  sf::RectangleShape m_request_shape;
  sf::Text m_request_text;
  ClientNode* m_requested_client;
  float m_timer_client;
  float m_time_client;
  int m_max_client;
  
  glm::vec2 m_document_pos;
  float m_document_dist;
  sf::Text m_document_text;
  int m_max_document;
  int m_document;
  int m_current_document;
private:
  AdminLevel( AdminLevel const& adminlevel ) = delete;
  AdminLevel& operator=( AdminLevel const& adminlevel ) = delete;
};

#endif
