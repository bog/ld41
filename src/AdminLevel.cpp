#include <AdminLevel.hpp>
#include <LevelNode.hpp>
#include <TileNode.hpp>
#include <ClientNode.hpp>
#include <PlayerNode.hpp>

AdminLevel::AdminLevel(LevelNode& level)
  : Node("ADMIN")
  , m_level (level)
{
  m_timer_client = 0;
  m_time_client = 2.0f;
  m_max_client = 2;
  m_requested_client = nullptr;  
  m_request_text.setCharacterSize(TILE_W * 0.7);
  m_request_text.setFillColor(sf::Color::Black);
  m_request_text.setFont(*$(fonts).pull("document"));
  m_request_shape.setSize({256, 128});
  m_request_shape.setFillColor(sf::Color::White);
  m_request_shape.setTexture($(textures).pull("tileset"));
  m_request_shape.setTextureRect({2*TILE_W, 0*TILE_H, 2*TILE_W, TILE_H});
  
  m_max_document = 2;
  m_document = 0;
  m_current_document = -1;
  for ( auto& tile : m_level.tiles() )
    {
      if (tile->id() == TILE_DESKTOP_1)
	{
	  m_document_pos = tile->grid_pos();
	  break;
	}      
    }
  
  m_document_pos.x *= TILE_W;
  m_document_pos.x += TILE_W/2;
  m_document_pos.y *= TILE_H;
  
  m_document_dist = TILE_W * 3;
  m_document_text.setCharacterSize(TILE_W);
  m_document_text.setFont(*$(fonts).pull("document"));
  m_document_text.setPosition({m_document_pos.x - m_document_text.getCharacterSize()/2,
	m_document_pos.y - m_document_text.getCharacterSize()/2});
  
}

void AdminLevel::draw(sf::RenderTarget& target,
		    sf::RenderStates states) const
{
  for (auto& client : m_clients)
    {
      target.draw(*client.get());
    }

  if (m_requested_client)
    {
      target.draw(m_request_shape);
      target.draw(m_request_text);
    }

  target.draw(m_document_text);
}

void AdminLevel::update_request(float dt)
{
  float min_dist = 3 * TILE_H;
  m_requested_client = nullptr;
  for (auto& client : m_clients)
    {
      float dist = glm::length(m_level.player().position() -
			       client->position());
      if (dist <= min_dist)
	{
	  m_requested_client = client.get();


	  m_request_shape.setPosition({
	      client->position().x - m_request_shape.getSize().x/2,
		client->position().y -
		client->size().y/2 -
		m_request_shape.getSize().x/2
		- TILE_H/2,
		});

	  m_request_text.setString("I want document " +
				   std::to_string(m_requested_client->document()));
	  
	  m_request_text.setPosition({
	      m_request_shape.getPosition().x +
		(m_request_shape.getGlobalBounds().width -
		 m_request_text.getGlobalBounds().width)/2,
		m_request_shape.getPosition().y +
		(m_request_shape.getGlobalBounds().height -
		 m_request_text.getGlobalBounds().height)/2,
		});
	  min_dist = dist;
	}
    }
}

void AdminLevel::update(float dt)
{
  for (auto& client : m_clients)
    {
      client->update(dt);
    }
  
  update_request(dt);

  if (static_cast<int>( m_clients.size() ) < m_max_client)
    {
      if (m_timer_client > m_time_client)
	{
	  add_client();
	  m_timer_client = 0.0f;
	}
      
      m_timer_client += dt;
    }
  
  // Remove client
  auto it = std::remove_if(std::begin(m_clients), std::end(m_clients),
			   [&](auto& c) {
			     if (!c->live() && c.get() == m_requested_client)
			       {
				 m_requested_client = nullptr;
			       }
			     return !c->live() && c->position().y < 1.5 * TILE_H;
			   });
  m_clients.erase(it, std::end(m_clients));

  // update document
  if (m_document < 1) { m_document = m_max_document; }
  if (m_document > m_max_document) { m_document = 1; }
  m_document_text.setString(std::to_string(m_document));
  if ( glm::length(m_level.player().position() - m_document_pos) < m_document_dist )
    {
      m_document_text.setFillColor(sf::Color::Black);
    }
  else
    {
      m_document_text.setFillColor(sf::Color(0, 0, 0, 150));
    }
  
}

void AdminLevel::add_client()
{
  ClientNode* c = new ClientNode(m_level, m_clients);
  c->position(m_level.spawn_pos());
  std::uniform_int_distribution<int> uid(1, m_max_document);
  c->document(uid($(rand)));
  m_clients.push_back(std::unique_ptr<ClientNode>(c));
}

void AdminLevel::on_event(sf::Event event)
{
  // Document navigation
  if ( glm::length(m_level.player().position() - m_document_pos) < m_document_dist )
    {
      if (event.type == sf::Event::KeyPressed &&
	  event.key.code == sf::Keyboard::Left)
	{
	  m_document--;
	}

      if (event.type == sf::Event::KeyPressed &&
	  event.key.code == sf::Keyboard::Right)
	{
	  m_document++;
	}
      
      if (m_requested_client == nullptr &&
	  event.type == sf::Event::KeyPressed &&
	  event.key.code == sf::Keyboard::Space)
	{
	  m_current_document = m_document;
	}
    }

  // talk to client
   if (m_requested_client != nullptr &&
       event.type == sf::Event::KeyPressed &&
      event.key.code == sf::Keyboard::Space)
    {
      if ( m_current_document != m_requested_client->document() )
	{
	  m_requested_client->wait_more();
	}
      else
	{
	  m_requested_client->happy();	  
	}
    }
   
  for (auto& client : m_clients)
    {
      client->on_event(event);
    }
}

void AdminLevel::increase_difficulty()
{
  m_max_client *= 3.0f;
  m_max_document *= 3.0f;
}

AdminLevel::~AdminLevel()
{
  
}
