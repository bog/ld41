#ifndef ACTORNODE_HPP
#define ACTORNODE_HPP
#include <iostream>
#include <Core.hpp>
#include <LevelNode.hpp>

class Animation;

class ActorNode : public Node
{
public:
  ActorNode(std::string name, LevelNode& level);
  
  void draw(sf::RenderTarget& target,
	    sf::RenderStates states) const override;

  void apply_force(glm::vec2 force);
  void update(float dt) override;

  void move(glm::vec2 pos) { m_position += pos; }
  sf::FloatRect rect() const { return m_shape.getGlobalBounds(); }
  glm::vec2 position() const { return m_position; }
  void position(glm::vec2 pos) { m_position = pos; }
  glm::vec2 size() const { return m_size; }
  void hurt(float damages);
  Animation& anim() { return *m_anim.get(); }
  virtual ~ActorNode();
  
protected:
  LevelNode& m_level;
  sf::RectangleShape m_shape;
  glm::vec2 m_size;
  glm::vec2 m_position;
  float m_max_vel;
  glm::vec2 m_velocity;
  glm::vec2 m_acc;
  float m_move_acc;
  float m_hp;
  float m_max_hp;
  std::unique_ptr<Animation> m_anim;
  
private:
  ActorNode( ActorNode const& actornode ) = delete;
  ActorNode& operator=( ActorNode const& actornode ) = delete;
};

#endif
