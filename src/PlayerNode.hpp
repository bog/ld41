#ifndef PLAYERNODE_HPP
#define PLAYERNODE_HPP
#include <iostream>
#include <ActorNode.hpp>

class PlayerNode : public ActorNode
{
public:
  PlayerNode(LevelNode& level);
  void update(float dt) override;
  void update_shoot(float dt);
  glm::vec2 shoot_dir() const { return m_shoot_dir; }
  virtual ~PlayerNode();
  
protected:
  glm::vec2 m_shoot_dir;
  
private:
  PlayerNode( PlayerNode const& playernode ) = delete;
  PlayerNode& operator=( PlayerNode const& playernode ) = delete;
};

#endif
