#include <Animation.hpp>
#include <TileNode.hpp>

Animation::Animation(sf::RectangleShape& shape)
  : Node("ANIMATION")
  , m_shape (shape)
  , m_row (0)
  , m_col_start (0)
  , m_col_end (0)
  , m_time (1.0f)
  , m_timer (m_time)
  , m_frame (m_col_start)
{
  m_shape.setTexture($(textures).pull("tileset"));
}

void Animation::setup(int row, int col_start, int col_end, float time)
{
  m_row = row;
  m_col_start = col_start;
  m_col_end = col_end;
  m_time = time;

  m_shape.setTextureRect({
      m_frame*TILE_W,
	m_row*TILE_H,
	TILE_W,
	TILE_H});      
}

void Animation::draw(sf::RenderTarget& target,
		     sf::RenderStates states) const
{
  target.draw(m_shape);
}
  
void Animation::update(float dt)
{
  if (m_timer >= m_time)
    {
      m_shape.setTextureRect({
	  m_frame*TILE_W,
	    m_row*TILE_H,
	    TILE_W,
	    TILE_H});
      
      m_timer = 0.0f;

      m_frame++;

      if (m_frame > m_col_end)
	{
	  m_frame = m_col_start;
	}
    }

  m_timer += dt;
}

Animation::~Animation()
{
  
}
