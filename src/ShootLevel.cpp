#include <ShootLevel.hpp>
#include <LevelNode.hpp>
#include <PlayerNode.hpp>
#include <ClientNode.hpp>
#include <TileNode.hpp>
#include <AdminLevel.hpp>

int ShootLevel::ShootCount = 2;

ShootLevel::ShootLevel(LevelNode& level, AdminLevel& admin)
  : Node("SHOOT")
  , m_level (level)
{
  $(input).add_action("shoot", sf::Keyboard::Space);
  m_shoot_line.setPrimitiveType(sf::PrimitiveType::Lines);
  m_shoot_line.resize(2);
  m_shoot_length = m_level.grid_size().x * m_level.grid_size().y;
  m_shoot_impact.setRadius(16);
  m_shoot_impact.setTexture($(textures).pull("tileset"));
  m_shoot_impact.setTextureRect({2*TILE_W, 1*TILE_H, TILE_W, TILE_H});
  m_time_shoot = 0.2f;
  m_timer_shoot = 0.0f;
  m_visible_shoot = false;
  m_client_count = admin.clients().size();

  m_frustration_decrease = m_level.max_frustration()/m_client_count;
  
  // copy clients
  for ( auto& client : admin.clients() )
    {
      ClientNode* c = new ClientNode(m_level, m_clients);
      c->position(client->position());
      c->flee();
      m_clients.push_back(std::unique_ptr<ClientNode>(c));
    }

  // copy player old pos
  m_player_old_pos = m_level.player().position();

  m_time_end = 2.0f;
  m_timer_end = 0.0f;
  
  m_gun_sound.setBuffer(*$(sounds).pull("gun1"));
  m_reload_sound.setBuffer(*$(sounds).pull("reload"));
  m_reload_sound.play();
}

void ShootLevel::draw(sf::RenderTarget& target,
		      sf::RenderStates states) const
{
  for (auto& client : m_clients)
    {
      client->draw(target, states);
    }
  
  if (m_visible_shoot)
    {
      target.draw(m_shoot_line);
      target.draw(m_shoot_impact);
    }
}


void ShootLevel::update(float dt)
{  
  for (auto& client : m_clients)
    {
      client->update(dt);
    }
  
  if (m_timer_shoot > m_time_shoot)
    {
      m_visible_shoot = false;
      m_timer_shoot = 0;
    }

  m_timer_shoot += dt;

  // Remove clients
  auto it = std::remove_if(std::begin(m_clients), std::end(m_clients),
			   [&](auto& c) {
			     return !c->live() && c->position().y < 1.5 * TILE_H;
			   });
  m_clients.erase(it, std::end(m_clients));

  if (m_client_count < 1)
    {
      m_timer_end += dt;
    }
}

void ShootLevel::on_event(sf::Event event)
{
  for (auto& client : m_clients)
    {
      client->on_event(event);
    }

  if (event.type == sf::Event::KeyPressed)
    {
      if ( $(input).action("shoot") )
	{
	  glm::vec2 dir = m_level.player().shoot_dir();
	  glm::vec2 impact = shoot_line(m_level.player().position(),
					m_level.player().position() + dir * m_shoot_length,
					sf::Color(100, 100, 0, 200) );
	  shoot_impact(impact);
	}

    }
}

glm::vec2 ShootLevel::shoot_line(glm::vec2 p1, glm::vec2 p2, sf::Color color)
{
  m_visible_shoot = true;
  
  m_shoot_line[0].color = color;
  m_shoot_line[0].position = {p1.x, p1.y};

  m_shoot_line[1].color = color;
  m_shoot_line[1].position = {p2.x, p2.y};

  float i = 0.0f;
  float di = 0.05f;
  glm::vec2 dir = glm::normalize(p2 - p1);
  glm::vec2 pos = p1;
  
  while (i < m_shoot_length)
    {
      glm::vec2 grid_pos = pos;
      grid_pos.x /= TILE_W;
      grid_pos.y /= TILE_H;

      // Obstacles
      if ( grid_pos.x > 0 && grid_pos.x <= m_level.grid_size().x
	  && grid_pos.y > 0 && grid_pos.y <= m_level.grid_size().y
	   && m_level.tile(grid_pos.y, grid_pos.x).obstacle() )
	{
	  m_shoot_impact.setFillColor(sf::Color::Yellow);
	  break;
	}

      bool client_col = false;
      // Clients
      for (auto& client : m_clients)
	{

	  if (!(pos.x < client->position().x - client->size().x/2 ||
	      pos.x > client->position().x + client->size().x/2 ||
	      pos.y < client->position().y - client->size().y/2 ||
		pos.y > client->position().y + client->size().y/2))
	    {
	      bool alive_before = client->alive();
	      client->hurt(50.0);
	      client->apply_force(25600000.0f * dir);
	      m_shoot_impact.setFillColor(sf::Color::Red);	      
	      client_col = true;
	      if (alive_before && !client->alive())
		{
		  std::string snd = "die";
		  std::uniform_int_distribution<int> uid(1, 3);
		  snd += std::to_string(uid($(rand)));
		  m_die_sound.setBuffer(*$(sounds).pull(snd));
		  m_die_sound.play();
		  m_level.decrease_frustration(m_frustration_decrease);
		  m_client_count--;
		}
	      break;
	    }
	}

      if (client_col) { break; }
      
      pos += dir * di;
      i += di;
    }
  
  glm::vec2 end = p1 + dir * i;
  m_shoot_line[1].position = {end.x, end.y};
  m_gun_sound.play();
  return end;
}

void ShootLevel::shoot_impact(glm::vec2 pos)
{
  m_shoot_impact.setPosition({pos.x - m_shoot_impact.getRadius()/2,
	pos.y - m_shoot_impact.getRadius()/2
	});
}

void ShootLevel::restore_player_pos()
{
  m_level.player().position(m_player_old_pos);
}

bool ShootLevel::ready_to_admin()
{
  return (m_client_count == 0 && m_timer_end > m_time_end);
}

ShootLevel::~ShootLevel()
{
  
}
