#ifndef RESOURCECACHE_HPP
#define RESOURCECACHE_HPP
#include <iostream>
#include <unordered_map>
#include <memory>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

template <typename T>
class ResourceCache
{
public:
  ResourceCache() {}

  virtual T* create_resource(std::string path) = 0;
  
  void push(std::string id, std::string path)
  {
    auto it = m_resources.find(id);

    if ( it != std::end(m_resources) )
      {
	throw std::runtime_error("Cannot push resource " + id);
      }

    T* resource = create_resource(path);
    
    m_resources[id] = std::unique_ptr<T>(resource);
  }

  T* pull(std::string id)
  {
    auto it = m_resources.find(id);

    if ( it == std::end(m_resources) )
      {
	throw std::runtime_error("Cannot pull resource " + id);
      }

    return m_resources[id].get();
  }
  
  virtual ~ResourceCache() {}
  
protected:
  std::unordered_map<std::string, std::unique_ptr<T>> m_resources;
private:
  ResourceCache( ResourceCache const& resourcecache ) = delete;
  ResourceCache& operator=( ResourceCache const& resourcecache ) = delete;
};

class FontCache : public ResourceCache<sf::Font>
{
public:
  sf::Font* create_resource(std::string path) override
  {
    sf::Font* font = new sf::Font;
    font->loadFromFile(path);
    return font;
  }  
};

class TextureCache : public ResourceCache<sf::Texture>
{
public:
  sf::Texture* create_resource(std::string path) override
  {
    sf::Texture* texture = new sf::Texture;
    texture->loadFromFile(path);
    return texture;
  }  
};

class MusicCache : public ResourceCache<sf::Music>
{
public:
  sf::Music* create_resource(std::string path) override
  {
    sf::Music* music = new sf::Music;
    music->openFromFile(path);
    return music;
  }  
};

class SoundCache : public ResourceCache<sf::SoundBuffer>
{
public:
  sf::SoundBuffer* create_resource(std::string path) override
  {
    sf::SoundBuffer* sound = new sf::SoundBuffer;
    sound->loadFromFile(path);
    return sound;
  }  
};  
#endif
