#ifndef TILENODE_HPP
#define TILENODE_HPP
#include <iostream>
#include <Core.hpp>
#include <ActorNode.hpp>

#define TILE_W 32
#define TILE_H 32

enum TileType {
  TILE_FLOOR = 1,
  TILE_WALL = 2,
  TILE_DESKTOP_1 = 21,
  TILE_DESKTOP_2 = 22
};

class TileNode : public ActorNode
{
public:
  TileNode(int i, int j,
	   sf::IntRect texture_rect,
	   bool obstacle, LevelNode& level);
  void draw(sf::RenderTarget& target,
	    sf::RenderStates states) const override;

  bool obstacle() const { return m_obstacle; }
  glm::vec2 grid_pos() const { return m_grid_pos; }
  virtual int id() const { return -1; };
  virtual ~TileNode();
  
protected:
  sf::RectangleShape m_shape;
  bool m_obstacle;
  glm::vec2 m_grid_pos;
private:
  TileNode( TileNode const& tilenode ) = delete;
  TileNode& operator=( TileNode const& tilenode ) = delete;
};

struct FloorTileNode : public TileNode
{
  FloorTileNode(int i, int j, LevelNode& level)
    : TileNode(i, j, {0, 0, 0, 0}, false, level)
  {
  }
  int id() const { return TILE_FLOOR; };
};


struct WallTileNode : public TileNode
{
  WallTileNode(int i, int j, LevelNode& level)
    : TileNode(i, j, {1, 0, 0, 0}, true, level)
  {
  }
  int id() const { return TILE_WALL; };
};

struct Desktop1TileNode : public TileNode
{
  Desktop1TileNode(int i, int j, LevelNode& level)
    : TileNode(i, j, {0, 1, 0, 0}, true, level)
  {
  }
  int id() const { return TILE_DESKTOP_1; };
};

struct Desktop2TileNode : public TileNode
{
  Desktop2TileNode(int i, int j, LevelNode& level)
    : TileNode(i, j, {1, 1, 0, 0}, true, level)
  {
  }
  int id() const { return TILE_DESKTOP_2; };
};
#endif
