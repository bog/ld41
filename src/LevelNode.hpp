#ifndef LEVELNODE_HPP
#define LEVELNODE_HPP
#include <iostream>
#include <Core.hpp>

class ActorNode;
class TileNode;
class PlayerNode;

class LevelNode : public Node
{
public:
  LevelNode();
  void draw(sf::RenderTarget& target,
	    sf::RenderStates states) const override;

  void update(float dt) override;
  void update_view();
  void update_collisions();  
  void on_event(sf::Event event) override;
  void load_map();
  void add_tile(int id, int i, int j);
  TileNode& tile(int i, int j);
  std::vector<std::unique_ptr<TileNode>>& tiles() { return m_tiles; }
  glm::vec2 grid_size() const;
  glm::vec2 spawn_pos() const { return m_spawn_pos; }
  PlayerNode& player();
  void increase_frustration();
  void decrease_frustration(float fr);
  float max_frustration() const { return m_max_frustration; }
  void shoot_mode();
  void admin_mode();
  virtual ~LevelNode();
  
protected:
  std::unique_ptr<Node> m_admin;
  std::unique_ptr<Node> m_shoot;
  Node* m_mode;
  std::unique_ptr<ActorNode> m_player;
  std::vector<std::unique_ptr<TileNode>> m_tiles;
  sf::View m_view;
  glm::vec2 m_size;
  float m_frustration;
  float m_max_frustration;
  glm::vec2 m_spawn_pos;
  sf::RectangleShape m_frustration_shape;
  sf::RectangleShape m_frustration_bg;
  float m_frustration_shape_height;
  bool m_frustration_visible;
  float m_frustration_gain;
  float m_frustration_ratio;
  int m_shoot_max;
  bool m_must_quit;
private:
  LevelNode( LevelNode const& levelnode ) = delete;
  LevelNode& operator=( LevelNode const& levelnode ) = delete;
};

#endif
