#include <fstream>
#include <sstream>
#include <cassert>
#include <LevelNode.hpp>
#include <AdminLevel.hpp>
#include <ShootLevel.hpp>
#include <PlayerNode.hpp>
#include <TileNode.hpp>
#include <EndNode.hpp>

LevelNode::LevelNode()
  : Node ("LEVEL")
{
  load_map();

  m_spawn_pos.y = TILE_H + TILE_H/2;
  
  for (size_t j=0; j<grid_size().x; j++)
    {
      if (!tile(1, j).obstacle())
	{
	  m_spawn_pos.x = j * TILE_W + TILE_W/2;
	  break;
	}
    }
  
  m_view = sf::View({0, 0, $(winsize).x, $(winsize).y});
  m_max_frustration = 100.0f;
  m_frustration_ratio = 0.8;
  m_frustration = 0;
  m_frustration_gain = 15.0f;
  m_frustration_shape_height = $(winsize).y * m_frustration_ratio;
  m_frustration_shape.setSize({$(winsize).x/16,
	m_frustration_shape_height});
  m_frustration_shape.setPosition({$(winsize).x -
	m_frustration_shape.getSize().x * 1.5f,
	$(winsize).y * (1.0f - m_frustration_ratio)/2.0f});
  m_frustration_shape.setFillColor(sf::Color::Red);
  m_frustration_visible = true;
  m_frustration_bg.setSize(m_frustration_shape.getSize());
  m_frustration_bg.setPosition(m_frustration_shape.getPosition());
  m_frustration_bg.setFillColor(sf::Color(150, 150, 150));
  m_player = std::unique_ptr<ActorNode>( new PlayerNode(*this) );
  
  m_admin = std::unique_ptr<Node>(new AdminLevel(*this));
  m_shoot = nullptr;
  m_mode = m_admin.get();
  m_shoot_max = 3;
  m_must_quit = false;
  
  $(musics).pull("radio_one")->play();
  $(musics).pull("radio_one")->setLoop(true);
  $(musics).pull("shoot")->play();
  $(musics).pull("shoot")->setLoop(true);
  $(musics).pull("shoot")->setVolume(0.0f);
  $(musics).pull("ambiant")->play();
  $(musics).pull("ambiant")->setLoop(true);
  $(musics).pull("ambiant")->setVolume(70.0f);
  
  ShootLevel::reset_shoot_count();
}

void LevelNode::draw(sf::RenderTarget& target,
	  sf::RenderStates states) const
{
  sf::View old_view = target.getView();

  target.setView(m_view);
  
  for (auto& tile : m_tiles)
    {
      target.draw(*tile.get());
    }

  if (m_mode)
    {
      target.draw(*m_mode);
    }
  
  target.draw(*m_player.get());
  target.setView(old_view);

  target.draw(m_frustration_bg);
  target.draw(m_frustration_shape);
}

void LevelNode::update(float dt)
{
  m_player->update(dt);

  if (m_mode && m_mode == m_admin.get())
    {
      $(musics).pull("radio_one")->setVolume(50.0f + (100.0f
						      - m_frustration)/2.0f);
      $(musics).pull("shoot")->setVolume(50.f + m_frustration/2.0f);
    }
  
  if (m_mode)
    {
      m_mode->update(dt);
    }
  
  for (auto& tile : m_tiles)
    {
      tile->update(dt);
    }

  update_collisions();
  update_view();

  if ( m_mode == m_shoot.get()
       && m_mode
       && static_cast<ShootLevel*>(m_shoot.get())->ready_to_admin() )
    {
      admin_mode();
    }
  
  m_frustration_shape.setSize({m_frustration_shape.getSize().x,
	(m_frustration/m_max_frustration) * m_frustration_shape_height});
  m_frustration_shape.setPosition({m_frustration_shape.getPosition().x,
	m_frustration_bg.getPosition().y +
	(1.0f - (m_frustration/m_max_frustration)) * m_frustration_shape_height
	});

  if (m_must_quit)
    {
      Core::instance().scene(new EndNode);
    }
}

void LevelNode::update_view()
{
  m_view.setCenter({m_player->position().x, m_player->position().y});
}

void LevelNode::update_collisions()
{
  // Player - Tile collisions
  for (auto& tile : m_tiles)
    {
      sf::FloatRect overlap;
      if ( tile->obstacle()
	   && m_player->rect().intersects(tile->rect(), overlap) )
	{
	  glm::vec2 pos = {-overlap.width, -overlap.height};
	  if (overlap.width > overlap.height) { pos.x = 0; }
	  else { pos.y = 0; }
	  if (overlap.left > tile->position().x) { pos.x *= -1; }
	  if (overlap.top > tile->position().y) { pos.y *= -1; }
	  m_player->move(pos);
	}
    }
}

void LevelNode::on_event(sf::Event event)
{
  m_player->on_event(event);

  if (m_mode)
    {
      m_mode->on_event(event);
    }
  
  for (auto& tile : m_tiles)
    {
      tile->on_event(event);
    }
}

void LevelNode::load_map()
{
  std::ifstream file("../assets/map.tmx");
  if (!file)
    {
      std::cerr<< "Cannot load map" <<std::endl;
      exit(-1);
    }
  
  std::string line;
  int i = 0;
  int j = 0;
  
  while ( std::getline(file, line) )
    {      
      if (line[0] != '<' && line[0] != ' ')
	{
	  j = 0;
	  std::string str_n;
	  for (size_t k=0; k<line.size(); k++)
	    {	      
	      if (line[k] == ',' || k == line.size() - 1)
		{
		  int n;
		  std::stringstream s;
		  s << str_n;
		  s >> n;
		  add_tile(n, i, j);
		  str_n = "";
		  j++;
		}
	      else
		{
		  str_n += line[k];
		}
	    }
	  i++;
	}
    }

  m_size.x = j * TILE_W;
  m_size.y = i * TILE_H;
}

void LevelNode::add_tile(int id, int i, int j)
{
  TileNode* tile;
  int W = $(winsize).x/TILE_W;
	    
  switch (id)
    {
      case TILE_FLOOR: tile = new FloorTileNode(i, j, *this); break;
      case TILE_WALL: tile = new WallTileNode(i, j, *this); break;
      case TILE_DESKTOP_1: tile = new Desktop1TileNode(i, j, *this); break;
      case TILE_DESKTOP_2: tile = new Desktop2TileNode(i, j, *this); break;
  	// Desktop
      case 5:
      case 6:
      case 7:
  	tile = new TileNode(i, j, {id, 0, 0, 0}, true, *this);
  	break;
      case 25:
      case 26:
      case 27:
	tile = new TileNode(i, j, {id - W - 1, 1, 0, 0}, false, *this); break;
  	break;
	
      default:
  	std::cerr<< "Tile " << id << " not found" <<std::endl;
  	exit(-1);
  	break;
    }

  m_tiles.push_back(std::unique_ptr<TileNode>(tile));
}

TileNode& LevelNode::tile(int i, int j)
{
  int w = m_size.x/TILE_W;
  return *m_tiles[i * w + j].get();
}

glm::vec2 LevelNode::grid_size() const
{
  return {m_size.x/TILE_W, m_size.y/TILE_H};
}

PlayerNode& LevelNode::player()
{
  return *static_cast<PlayerNode*>(m_player.get());
}

void LevelNode::increase_frustration()
{
  m_frustration += m_frustration_gain;
  
  if (m_frustration >= m_max_frustration)
    {
      m_frustration = m_max_frustration;
      shoot_mode();
    }
}

void LevelNode::decrease_frustration(float fr)
{
  m_frustration -= fr;

  if (m_frustration <= 0)
    {
      m_frustration = 0;
    }
}

void LevelNode::shoot_mode()
{
  $(musics).pull("radio_one")->setVolume(0.0f);
  static_cast<ShootLevel*>(m_shoot.get())->new_shoot_mode();
    
  m_shoot =
    std::unique_ptr<Node>(
			  new ShootLevel(*this,
					 *static_cast<AdminLevel*>(m_admin.get())));
  m_mode = m_shoot.get();
}

void LevelNode::admin_mode()
{
  if (static_cast<ShootLevel*>(m_shoot.get())->shoot_count() < m_shoot_max)
    {
      static_cast<ShootLevel*>(m_shoot.get())->restore_player_pos();
      m_shoot.reset(nullptr);
      m_mode = m_admin.get();
      static_cast<AdminLevel*>(m_admin.get())->increase_difficulty();
    }
  else
    {
      m_must_quit = true;
      $(musics).pull("radio_one")->stop();
      $(musics).pull("ambiant")->stop();
    }
}

LevelNode::~LevelNode()
{
  
}
