#include <ClientNode.hpp>
#include <TileNode.hpp>
#include <Animation.hpp>

ClientNode::ClientNode(LevelNode& level, std::vector<std::unique_ptr<ClientNode>>& others)
  : ActorNode("CLIENT", level)
  , m_others (others)
{
  m_max_vel /= 2;

  m_state = ClientState::Arrive;
  m_avoid_dist = 1.5 * TILE_H;
  m_live = true;
  
  std::uniform_int_distribution<int> uid(10, 30);
  m_time_wait = uid($(rand));
  m_timer_wait = 0;
  m_angle = 0.0f;
  m_timer_flee = 0.0f;
  m_time_flee = 0.5f;

  m_wait_shape.setFillColor(sf::Color::Red);
  m_anim->setup(2, 2, 2, 0.2f);
  m_timer_stay = 0.0f;
  m_time_stay = 0.7f;
}

void ClientNode::update(float dt)
{
  if ( alive() )
    {
      update_avoid(dt);
    }
  else
    {
      m_anim->setup(2, 2, 2, 0.2f);
    }
  
  if ( glm::length(m_velocity) >= 128 && alive() )
    {
      if (m_velocity.x < 0)
	{
	  m_anim->setup(6, 0, 1, 0.2f);
	}

      if (m_velocity.x > 0)
	{
	  m_anim->setup(4, 0, 1, 0.2f);
	}

      if (m_velocity.y < 0)
	{
	  m_anim->setup(3, 0, 1, 0.2f);
	}

      if (m_velocity.y > 0)
	{
	  m_anim->setup(5, 0, 1, 0.2f);
	}

      if (m_timer_stay > m_time_stay)
	{
	  m_anim->setup(2, 2, 2, 0.2f);
	  m_timer_stay = 0.0f;
	}
    }
  
  if (glm::length(m_velocity) == 0)
    {
      m_timer_stay += dt;
    }
    
  switch (m_state)
    {
      case ClientState::Arrive:
	update_arrive(dt);
	break;

      case ClientState::Wait:
	update_wait(dt);
	break;

      case ClientState::Leave:
	update_leave(dt);
	break;

      case ClientState::Dead:
	m_shape.setFillColor(sf::Color(150, 150, 150));
	break;

      case ClientState::Flee:
	update_flee(dt);
	break;
      default:break;
    }

  if (m_hp <= 0)
    {
      m_state = ClientState::Dead;
    }
  
  update_collisions(dt);
  
  ActorNode::update(dt);

  m_wait_shape.setSize({m_size.x * ((m_time_wait - m_timer_wait)/m_time_wait), m_size.y/8.0f});
  m_wait_shape.setPosition({m_position.x - m_size.x/2,
	m_position.y - m_size.y * 0.7f});
}

void ClientNode::update_flee(float dt)
{
  if (m_timer_flee >= m_time_flee)
    {
      std::uniform_real_distribution<float> urd(0, 2*M_PI);
      m_angle = urd($(rand));
      m_timer_flee = 0.0f;
    }

  glm::vec2 dir;
  dir = {cos(m_angle), sin(m_angle)};
  apply_force(m_move_acc * dir);
  m_timer_flee += dt;
}

void ClientNode::update_avoid(float dt)
{
  glm::vec2 avoid = {0, 0};
  int count = 0;
  for (auto& client : m_others)
    {
      if (client.get() == this) { continue; }
      glm::vec2 to_client = (m_position - client->position());
      
      if (glm::length(to_client) < m_avoid_dist)
	{
	  count++;
	  avoid += (m_position - client->position());
	  std::uniform_int_distribution<int> uid(-5, 5);
	  avoid.x += uid($(rand));
	}
    }
  if (count > 0)
    {
      avoid = glm::normalize(avoid);
      apply_force(m_move_acc * avoid);
    }
}

void ClientNode::update_collisions(float dt)
{
  for ( auto& tile : m_level.tiles() )
    {
      sf::FloatRect overlap;
      if ( tile->obstacle()
	   && rect().intersects(tile->rect(), overlap) )
	{
	  glm::vec2 pos = {-overlap.width, -overlap.height};
	  if (overlap.width > overlap.height) { pos.x = 0; }
	  else { pos.y = 0; }
	  if (overlap.left > tile->position().x) { pos.x *= -1; }
	  if (overlap.top > tile->position().y) { pos.y *= -1; }
	  move(pos);
	}
    }
}

void ClientNode::update_leave(float dt)
{
  static glm::vec2 obj = {m_position.x + TILE_W, m_position.y - TILE_H};
  static int count = 0;
  
  glm::vec2 to_obj = glm::normalize(obj - m_position);
  
  apply_force(m_move_acc * to_obj);
  
  float dist = 8.0f;
  if (count == 0 && glm::length(obj - m_position) <= dist)
    {
      obj.y = TILE_H/2;
  
      for (size_t j=0; j<m_level.grid_size().x; j++)
	{
	  if (!m_level.tile(0, j).obstacle())
	    {
	      obj.x = j * TILE_W + TILE_W/2;
	      break;
	    }
	}
      
      count = 1;
    }

  if (count == 1)
    {
      m_live = false;
    }
}

void ClientNode::update_wait(float dt)
{
  if (m_timer_wait > m_time_wait)
    {
      m_state = ClientState::Leave;
      m_timer_wait = 0.0f;
    }
  
  m_timer_wait += dt;
}

void ClientNode::update_arrive(float dt)
{
  bool no_obstacle = m_position.y < TILE_H ||
				    !m_level.tile((m_position.y + TILE_H/2)/TILE_H,
						  m_position.x/TILE_W).obstacle();
    
  if (no_obstacle)
    {
      apply_force({0, m_move_acc});
    }
  else
    {     
      m_state = ClientState::Wait;
    }

}

void ClientNode::draw(sf::RenderTarget& target,
		      sf::RenderStates states) const
{
  ActorNode::draw(target, states);

  if (m_state == ClientState::Wait)
    {
      target.draw(m_wait_shape);
    }
}

void ClientNode::happy()
{
  m_state = ClientState::Leave;
  m_level.increase_frustration();
}

ClientNode::~ClientNode()
{
  
}
