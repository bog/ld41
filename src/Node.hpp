#ifndef NODE_HPP
#define NODE_HPP
#include <iostream>
#include <SFML/Graphics.hpp>

class Node : public sf::Drawable
{
public:
  Node(std::string name)
    : m_name (name)
  {
  }

  virtual void draw(sf::RenderTarget& target,
		    sf::RenderStates states) const override = 0;

  virtual void update(float dt) {}
  virtual void on_event(sf::Event event) {}
  
  virtual ~Node() {}
  
protected:
  std::string m_name;
  
private:
  Node( Node const& node ) = delete;
  Node& operator=( Node const& node ) = delete;
};

#endif
