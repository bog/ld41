#ifndef CORE_HPP
#define CORE_HPP
#include <iostream>
#include <glm/glm.hpp>
#include <chrono>
#include <random>
#include <Node.hpp>
#include <InputSystem.hpp>
#include <ResourceCache.hpp>

#define $(X) Core::instance().X()

class Core : public Node
{
public:

  static Core& instance()
  {
    if (!m_instance)
      {
	m_instance = new Core();
      }

    return *m_instance;
  }

  static void destroy()
  {
    delete m_instance;
    m_instance = nullptr;
  }
  
  void draw(sf::RenderTarget& target,
	    sf::RenderStates states) const override;

  void update(float dt) override;
  void on_event(sf::Event event) override;
  void run(sf::RenderWindow* window);
  
  virtual ~Core();

  InputSystem& input() { return *m_input.get(); }
  TextureCache& textures() { return *m_textures.get(); }
  FontCache& fonts() { return *m_fonts.get(); }
  MusicCache& musics() { return *m_musics.get(); }
  SoundCache& sounds() { return *m_sounds.get(); }
  void scene(Node* node) { m_scene.reset(node); }
  glm::vec2 winsize() const;
  std::mt19937& rand() { return m_rand; }
  void quit() { m_running = false; }
protected:
  static Core* m_instance;
  std::unique_ptr<Node> m_scene;
  sf::RenderWindow* m_window;
  std::unique_ptr<InputSystem> m_input;
  std::unique_ptr<TextureCache> m_textures;
  std::unique_ptr<FontCache> m_fonts;
  std::unique_ptr<MusicCache> m_musics;
  std::unique_ptr<SoundCache> m_sounds;
  std::mt19937 m_rand;
  bool m_running;
  Core();
  
private:
  Core( Core const& core ) = delete;
  Core& operator=( Core const& core ) = delete;
};

#endif
