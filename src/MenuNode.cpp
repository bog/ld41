#include <MenuNode.hpp>
#include <LevelNode.hpp>

MenuNode::MenuNode()
  : Node("MENU")
{
  m_shape.setSize({$(winsize).x, $(winsize).y});
  m_shape.setTexture($(textures).pull("menu"));

  $(musics).pull("menu")->play();
}

void MenuNode::draw(sf::RenderTarget& target,
		    sf::RenderStates states) const
{
  target.draw(m_shape);
}

void MenuNode::update(float dt)
{
}

void MenuNode::on_event(sf::Event event)
{
  if (event.type == sf::Event::KeyPressed)
    {
      if (event.key.code == sf::Keyboard::Space)
	{
	  $(musics).pull("menu")->stop();
	  Core::instance().scene(new LevelNode);
	}
      if (event.key.code == sf::Keyboard::Escape)
	{
	  $(musics).pull("menu")->stop();
	  $(quit);
	}
    }
}

MenuNode::~MenuNode()
{
  
}
