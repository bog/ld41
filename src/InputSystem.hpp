#ifndef INPUTSYSTEM_HPP
#define INPUTSYSTEM_HPP
#include <iostream>
#include <unordered_map>
#include <vector>

class InputSystem
{
public:
  InputSystem() {}

  void add_action(std::string id, sf::Keyboard::Key key)
  {
    m_keys[id].push_back(key);
  }

  bool action(std::string id)
  {
    for (auto& key : m_keys[id])
      {
	if ( sf::Keyboard::isKeyPressed(key) )
	  {
	    return true;
	  }
      }

    return false;
  }
  
  virtual ~InputSystem() {}
  
protected:
  std::unordered_map<std::string, std::vector<sf::Keyboard::Key>> m_keys;
  
private:
  InputSystem( InputSystem const& inputsystem ) = delete;
  InputSystem& operator=( InputSystem const& inputsystem ) = delete;
};

#endif
