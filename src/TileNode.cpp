#include <TileNode.hpp>
#include <Animation.hpp>

TileNode::TileNode(int i, int j, sf::IntRect texture_rect, bool obstacle, LevelNode& level)
  : ActorNode("TILE", level)
  , m_obstacle (obstacle)
  , m_grid_pos ({j, i})
{
  m_size = {TILE_W, TILE_H};
  m_position = {static_cast<float>(j * TILE_W) + m_size.x/2,
		static_cast<float>(i * TILE_H) + m_size.y/2};

  m_shape.setFillColor(sf::Color::White);

  m_shape.setTexture($(textures).pull("tileset"));
  m_shape.setTextureRect(texture_rect);
  m_shape.setSize({m_size.x, m_size.y});
  m_shape.setPosition({m_position.x - m_size.x/2, m_position.y - m_size.y/2});

  m_anim->setup(texture_rect.top,
  		texture_rect.left,
  		texture_rect.left,
  		0.0f);
}

void TileNode::draw(sf::RenderTarget& target,
		    sf::RenderStates states) const
{
  ActorNode::draw(target, states);
}

TileNode::~TileNode()
{
  
}
