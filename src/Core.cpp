#include <chrono>
#include <Core.hpp>
#include <LevelNode.hpp>
#include <MenuNode.hpp>
#include <EndNode.hpp>

Core* Core::m_instance = nullptr;

Core::Core()
  : Node ("CORE")
  , m_running (true)
{
  m_input = std::make_unique<InputSystem>();
  
  std::string assets = "../assets/";
  // texture
  m_textures = std::make_unique<TextureCache>();  
  m_textures->push("tileset", assets + "tileset.png");
  m_textures->push("menu", assets + "menu.png");
  m_textures->push("end", assets + "end.png");
  
  // font
  m_fonts = std::make_unique<FontCache>();
  m_fonts->push("document", assets + "LiberationMono-Bold.ttf");
  // music
  m_musics = std::make_unique<MusicCache>();
  m_musics->push("radio_one", assets + "radio_one.ogg");
  m_musics->push("menu", assets + "menu.ogg");
  m_musics->push("ambiant", assets + "ambiant.ogg");
  m_musics->push("shoot", assets + "shoot.ogg");
  // sound
  m_sounds = std::make_unique<SoundCache>();
  m_sounds->push("gun1", assets + "gun1.ogg");
  m_sounds->push("reload", assets + "reload.ogg");
  m_sounds->push("die1", assets + "die1.ogg");
  m_sounds->push("die2", assets + "die2.ogg");
  m_sounds->push("die3", assets + "die3.ogg");
  
}

void Core::draw(sf::RenderTarget& target,
	    sf::RenderStates states) const
{
  target.clear(sf::Color::Black);

  if (m_scene)
    {
      m_window->draw(*m_scene.get());
    }
}

void Core::update(float dt)
{
  if (m_scene)
    {
      m_scene->update(dt);
    }
}

void Core::on_event(sf::Event event)
{
  switch (event.type)
    {
      case sf::Event::Closed:
	m_window->close();
	m_running = false;
	break;

      default: break;
    }

  if (m_scene)
    {
      m_scene->on_event(event);
    }
}

void Core::run(sf::RenderWindow* window)
{
  m_window = window;
  m_scene = std::unique_ptr<Node>(new MenuNode);
  
  sf::Event event;
  std::chrono::duration<float> elapsed;
  
  while ( m_running )
    {
      std::chrono::steady_clock::time_point loop_start =
	std::chrono::steady_clock::now();
	
      while ( window->pollEvent(event) )
	{
	  on_event(event);
	}

      update( elapsed.count() );
      
      window->draw(*this);
      window->display();

      elapsed = std::chrono::steady_clock::now() - loop_start;
    }
}

glm::vec2 Core::winsize() const
{
  return {
    static_cast<float>(m_window->getSize().x),
      static_cast<float>(m_window->getSize().y)
  };
}

Core::~Core()
{
  
}
